---
# Banner
# Features
features:
  - title: "About"
    image: "/images/sappoham.jpg"
    content: "I'm a computer engineer from the beautiful landscapes of Nepal, with interest in technologies and a deep appreciation for peace and quiet. My journey began with a fascination for Linux Systems, and gradually expanding to System Administration, Networking and DevOps expanding into Python programming, cybersecurity and the intricate world of Engineering Management. My interests are playing around with IoT devices and Single Board Computers, all with a passion for self-hosting.\n\n
    ___
    \n \n Beyond the screen, you'll find me exploring off-the-grid terrains, and exploring the hilltop vistas. Lately, I've found myself drawn to embracing the insights of Yoga philosophy. Also, let's not forget my special talent for mastering the art of procrastination - because even in the fast-paced world of technology, a good sleep is non-negotiable.\n\n
    ___
    \n\nJoin me on this journey as I try to blend the realms of technology and laziness, one kernel panic at a time."
    

---
